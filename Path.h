#pragma once
#include<iostream>
#include"Node.h"
using namespace std;

class Path {
	friend ostream &operator<<(ostream&, Pose	 	&);
	friend istream &operator>>(istream&, Path&);

private:
	
	Node * tail;
    Node * head;
	int number;
	
public:

	Path(Node*tail = NULL, Node*head = NULL, int number = 0){}
	~Path(){}
	void addPos(Pose);
	void print();
	Pose operator[](int);
	Pose getPos(int);
	bool removePos(int);
	bool insertPos(int, Pose);
	int getnumber();
	

};