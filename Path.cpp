#include"Path.h"
#include<iostream>

using namespace std;


void Path::addPos(Pose pose)
{
	if (head == NULL) 
	{
		head = new Node();
		head->pose = pose;
		head->next = NULL;
		tail = head;
		number++;
	}
	else
	{
		Node * temp = head;
		while (head->next != NULL)
		{
			head = head->next;
		}
		head->next = new Node(); ///Verilerin sonuna yeni bir alan olusturur.
		head = head->next;
		head->pose = pose;
		head->next = NULL;///Bu alanin sonu NULL belirlenir.
		tail = head;
		head = temp;
		number++;
	}

}
void Path::print()
{
	Node*temp = head;
	while (head != NULL) //NULL gorene kadar ilerlenir
	{
		cout << head->pose;
		head = head->next;    //diger verinin okunmasi saglanir.

	}
	head = temp;
}
Pose Path::operator[](int index)
{
	Node*temp = head;
	for (int i = 0; i < index ; i++) ///Bu alanin istenilen pozisyonu bulmasini saglar.
	{
		if (temp == NULL)
		{
			break;
		}
		temp = temp->next;
	}
	return temp->pose;
}
Pose Path::getPos(int index)
{
	Node*temp = head;
	

	for (int i = 0; i < index; i++) ///Bu alanin istenilen pozisyonu bulmasini saglar.
	{
		if (temp == NULL)
		{
			break;
		}
		temp = temp->next;
	}
	
	return temp->pose; ///istenilen pozisyon dondurulur

}
bool Path::removePos(int index) // tail son eleman silinirse tutma.
{
	Node*temp; ///Yeni bi tutucu olusturulur.
	Node *temp1 = head;

	if (number < index + 1) //eger index number degerinden buyuk olursa
	{
		//cout << "Error" << endl;
		return false;
	}
	if (index == 0)
	{
		head = head->next;
		number--;
		return true;
	}
	for (int i = 0; i < index - 1; i++) ///Bu alanin istenilen pozisyonu bulmasini saglar.
	{
		if (head == NULL)
		{
			break;
		}
		head = head->next;
	}
	
	if (head != NULL) {
		
		temp = head->next;
		head->next = head->next->next;
		free(temp);
		if (head->next==NULL)
		{
			tail = head;

		}
		head = temp1;
		number--;
		return true;
	}
	return false;
}
bool Path::insertPos(int index, Pose pose)
{
	index = index + 1; //bir sonraki indexe eklenmesi gerektiği icin.
	Node*temp = new Node(); ///Yeni bi alan olusturulur.
	Node*temp1 = head;
	if (number < index)
	{
		//cout << "Error" << endl;
		return false;
	}
	if (index == 0)
	{
		temp->pose = pose;
		temp->next = head;
		head = temp;
		number++;
		return true;
	}
	for (int i = 0; i < index - 1; i++) ///Bu alanin istenilen pozisyonu bulmasini saglar.
	{
		if (head == NULL)
		{
			break;
		}
		head = head->next;
	}
	if (head != NULL) {
		temp->pose = pose;
		temp->next = head->next;  ///Yeni alani istenilen pozisyona baglar.
		head->next = temp;
		
		head = temp1;
		number++;
		return true;
	}
	return false;
}
ostream & operator<<(ostream &out, Pose &pose)
{
	out << pose.getX() << "mm, " << pose.getY() << "mm, " << pose.getTh() << "' " << endl;
	return out;
}

istream & operator>>(istream &in, Path & path)
{
	
	float x, y, th;
	in >> x >> y >> th;
	Pose pose;
	pose.setPose(x, y, th);
	path.addPos(pose);

	return in;
}
int Path::getnumber()
{
	return number;
}
